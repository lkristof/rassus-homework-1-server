package com.example.demo.controller;

import com.example.demo.model.Measurement;
import com.example.demo.model.Sensor;
import com.example.demo.model.UserAddress;
import com.example.demo.service.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class SensorController {

    @Autowired
    private SensorService sensorService;

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public boolean register(@RequestBody Sensor sensor){
        return sensorService.register(sensor);
    }

    @RequestMapping("findNeighbour")
    public UserAddress searchNeighbour(@RequestHeader("username") String username) {
        return sensorService.findNeigbour(username);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/storeMeasurements")
    public boolean storeMeasurements(@RequestBody Measurement measurement){
        return sensorService.storeMeasurements(measurement.getUsername(), measurement.getParameter(), measurement.getValue());
    }
}
