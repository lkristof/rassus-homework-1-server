package com.example.demo.model;

public class UserAddress {
    String IPaddress;
    int port;

    public UserAddress(String IPaddress, int port) {
        this.IPaddress = IPaddress;
        this.port = port;
    }

    public UserAddress() {
    }

    public String getIPaddress() {
        return IPaddress;
    }

    public void setIPaddress(String IPaddress) {
        this.IPaddress = IPaddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
