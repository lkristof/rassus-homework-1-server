package com.example.demo.model;

import java.io.Serializable;
import java.util.Objects;

public class Sensor implements Serializable {
    String username;
    double latitude;
    double longitude;
    String ipaddress;
    int port;

    public Sensor(){

    }

    public Sensor(String username, double latitude, double longitude, String ipaddress, int port) {
        this.username = username;
        this.latitude = latitude;
        this.longitude = longitude;
        this.ipaddress = ipaddress;
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sensor)) return false;
        Sensor sensor = (Sensor) o;
        return Objects.equals(getUsername(), sensor.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername());
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "username='" + username + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", IPaddress='" + ipaddress + '\'' +
                ", port=" + port +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
