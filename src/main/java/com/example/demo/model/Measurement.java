package com.example.demo.model;

public class Measurement {
    String username;
    String parameter;
    float value;

    public Measurement() {
    }

    public Measurement(String username, String parameter, float value) {
        this.username = username;
        this.parameter = parameter;
        this.value = value;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "username='" + username + '\'' +
                ", parameter='" + parameter + '\'' +
                ", value=" + value +
                '}';
    }
}
