package com.example.demo.service;

import com.example.demo.model.Measurement;
import com.example.demo.model.Sensor;
import com.example.demo.model.UserAddress;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SensorService {
    List<Sensor> listofSensors = new ArrayList<>();
    List<Measurement> listOfMeasurements = new ArrayList<>();

    public boolean register(Sensor sensor){
        if(listofSensors.contains(sensor))
            return false;
        listofSensors.add(sensor);
        System.out.println(listofSensors.toString());
        return true;
    }

    public UserAddress findNeigbour(String username) {
        System.out.println("User " + username + " searches for neighbour");
        double d = -1;
        double latSensor = 0;
        double longSensor = 0;
        String sensorName = "";
        int R = 6371;

        for(Sensor sensor : listofSensors) {
            if (sensor.getUsername().equals(username)) {
                latSensor = sensor.getLatitude();
                longSensor = sensor.getLongitude();
            }
        }

        for(Sensor sensor : listofSensors){
            if(sensor.getUsername().equals(username))
                continue;
            double dlat = sensor.getLatitude() - latSensor;
            double dlon = sensor.getLongitude() - longSensor;
            double a = (Math.sin(dlat/2) * Math.sin(dlat/2) + Math.cos(latSensor) * Math.cos(sensor.getLatitude()) * Math.sin(dlon/2) * Math.sin(dlon/2) );
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            if(d == -1) {
                d = R * c;
                sensorName = sensor.getUsername();
            }
            else if (d > R*c){
                sensorName = sensor.getUsername();
                d = R*c;
            }
            else
                continue;
        }

        for(Sensor sensor : listofSensors)
            if(sensor.getUsername().equals(sensorName))
                return new UserAddress(sensor.getIpaddress(), sensor.getPort());

        return null;
    }

    public boolean storeMeasurements(String username, String parameter, float value) {
        listOfMeasurements.add(new Measurement(username, parameter, value));
        System.out.println(username + " stroing " + parameter + " with value " + value);
        System.out.println(listOfMeasurements);
        return true;
    }
}
